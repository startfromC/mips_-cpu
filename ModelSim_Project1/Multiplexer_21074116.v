//二选一多路选择器
module multiplexer2_1(
  input ctr,
  input [31:0] din0,
  input [31:0] din1,
  
  output reg [31:0] out
  );
  
  always@(*)
    begin
      if(ctr==1'b0) out=din0;
      else out=din1;
    end
endmodule

//三选一多路选择器
module multiplexer3_1(
  input [1:0]ctr,
  input [31:0] din0, //ALU的运算结果
  input [31:0] din1, //数据存储器的输出 lw
  input [31:0] din2, //jal指令，跳转前保存的地址，存入31号寄存器
  
  output reg [31:0] out
  );
  
  always@(*)
    begin
      if(ctr==2'b00)      out=din0;
      else if(ctr==2'b01) out=din1;
      else if(ctr==2'b10) out=din2;
    end
endmodule

//三选一多路选择器（5位），看上去是二选一，本质上是三选一
module multiplexer3_2(
  input [1:0] ctr,   
  input [4:0] din0,  //rt
  input [4:0] din1,  //rd
  
  output reg [4:0] out
  );
  
  always@(*)
    begin
      if(ctr==2'b00)      out=din0;
      else if(ctr==2'b01) out=din1;
      else if(ctr==2'b10) out=5'b1_1111; //jal指令跳转前，向31号寄存器存地址
    end
endmodule
  
