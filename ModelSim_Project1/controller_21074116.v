module controller(
  input [5:0] op,
  input zero,      //零标记
  input [5:0] func,
  input overflow,  //溢出标记
  input BLTZAL_ctr,
  
  output [1:0]Regdst,//GPR写使能有效时，选目标寄存器00-rt，01-rd，10-31号
  output ALUsrc,     //第二个操作数，0-GPR，1-Extender
  output [1:0] MemtoReg, //写入寄存器的数据来源00-ALU，01-DM,10-NPC
  output RegWrite,  //寄存器写使能
  output MemWrite,  //dm写使能
  output [1:0]ExtOp,//00-无符号，01-符号，10-高位置立即数
  output [2:0]ALUOp, //000-比较，001-或，010-无溢出加法，011-有溢出加法，100-减法,101-BLTZAL
  output flow_ctr,  //指示GPR是否更新溢出标记
  output [2:0]NPCop  //000-PC+4,001-beq&zero/BLTZAL,010-j/jal,011-jr
  );
  
  wire Rtype;
  wire addu,subu,ori,lw,sw,beq,j,lui,addi,addiu,slt,jal,jr,BLTZAL;
  assign Rtype=~op[5]&~op[4]&~op[3]&~op[2]&~op[1]&~op[0];
  assign addu=Rtype&func[5]&~func[4]&~func[3]&~func[2]&~func[1]&func[0];
  assign subu=Rtype&func[5]&~func[4]&~func[3]&~func[2]&func[1]&func[0];
  assign ori=~op[5]&~op[4]&op[3]&op[2]&~op[1]&op[0];
  assign lw=op[5]&~op[4]&~op[3]&~op[2]&op[1]&op[0];
  assign sw=op[5]&~op[4]&op[3]&~op[2]&op[1]&op[0];
  assign beq=~op[5]&~op[4]&~op[3]&op[2]&~op[1]&~op[0];
  assign j=~op[5]&~op[4]&~op[3]&~op[2]&op[1]&~op[0];
  assign lui=~op[5]&~op[4]&op[3]&op[2]&op[1]&op[0];
  assign addi=~op[5]&~op[4]&op[3]&~op[2]&~op[1]&~op[0];
  assign addiu=~op[5]&~op[4]&op[3]&~op[2]&~op[1]&op[0];
  assign slt=Rtype&func[5]&~func[4]&func[3]&~func[2]&func[1]&~func[0];
  assign jal=~op[5]&~op[4]&~op[3]&~op[2]&op[1]&op[0];
  assign jr=Rtype&~func[5]&~func[4]&func[3]&~func[2]&~func[1]&~func[0];
  assign BLTZAL=~op[5]&~op[4]&~op[3]&~op[2]&~op[1]&op[0];
  
  assign Regdst[0]=addu|subu|slt;
  assign Regdst[1]=jal|BLTZAL;
  assign ALUsrc=ori|lw|sw|lui|addi|addiu;
  assign MemtoReg[0]=lw;
  assign MemtoReg[1]=jal|BLTZAL;
  assign RegWrite=addu|subu|ori|lw|lui|addi&~overflow|addiu|slt|jal|BLTZAL&BLTZAL_ctr;
  assign MemWrite=sw;
  assign ExtOp[0]=lw|sw|addi|addiu;
  assign ExtOp[1]=lui;
  assign ALUOp[0]=ori|addi|lui|BLTZAL;
  assign ALUOp[1]=addu|lw|sw|addi|addiu;
  assign ALUOp[2]=subu|BLTZAL;
  assign flow_ctr=addi&overflow;
  assign NPCop[0]=beq&zero|jr|BLTZAL&BLTZAL_ctr;
  assign NPCop[1]=j|jal|jr;
  assign NPCop[2]=1'b0;

endmodule
        
        
          
                
                
      