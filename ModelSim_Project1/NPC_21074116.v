module NPC(
  input [31:0] PC,
  input [15:0] imm16, //beq跳转偏移
  input [25:0] imm26, //j,jal跳转地址
  input [31:0] rs,    //jr跳转地址存的寄存器
  input [2:0]NPCop,  //000-PC+4,001-beq&zero,010-j/jal,011-jr
  
  output reg [31:0] NPC,  //下一条指令地址
  output [31:0] jalAd     //jal跳转前向31号寄存器存的地址
  );
  
  assign jalAd=PC+4;     
  reg [31:0] imm16_exed; //imm16符号扩展+00之后的值
  
  always@(*)
    begin
      if(NPCop==3'b010) NPC<={PC[31:28],imm26,2'b0}; //j/jal
      else if(NPCop==3'b011) NPC<=rs;    //jr
      else if(NPCop==3'b001)     //beq
        begin
          if(imm16[15]==1'b0) imm16_exed={14'b00_0000_0000_0000,imm16[15:0],2'b0};
          else if(imm16[15]==1'b1) imm16_exed={14'b11_1111_1111_1111,imm16[15:0],2'b0};
            NPC<=PC+4+imm16_exed;
        end
      else if(NPCop==3'b000) NPC<=PC+4;  //PC+4
    end
endmodule