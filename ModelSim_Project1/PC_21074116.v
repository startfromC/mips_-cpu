module PC(
  input [31:0] NPC,   //要输出的指令地址
  input clk,
  input reset,
  output reg [31:0] PC  //输出的指令地址，即当前指令
  );
  
  initial
    begin
      PC<=32'h0000_3000;
    end
    
  always@(posedge clk,posedge reset)
    begin
      if(reset) PC<=32'h0000_3000;
      else PC<=NPC;
    end
endmodule