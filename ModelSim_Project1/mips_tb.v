module mips_tb;
	reg clk;
	reg reset;
	
	initial 
	 begin
	   clk = 1;
	   reset=0;
	   #5 reset=1;
	   #5 reset=0;
     $readmemh("mips1.txt",mymips.i1.im); //�ļ���ȡָ��
	 end
	   
	always #20 clk = ~clk;
		
		mips mymips(clk, reset);
endmodule
