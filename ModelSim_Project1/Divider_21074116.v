//把32位指令分开的装置
module Divider(
  input [31:0] ins,
  
  output [5:0]op,
	output [4:0]rs,
	output [4:0]rt,
	output [4:0]rd,
	output [5:0]func,
	output [15:0]imm16,//beq跳转偏移
	output [25:0]imm26//j、jal指令 26位跳转地址
	);
	
	assign op=ins[31:26];
	assign rs=ins[25:21];
	assign rt=ins[20:16];
	assign rd=ins[15:11];
  assign func=ins[5:0];
	assign imm16=ins[15:0];
	assign imm26=ins[25:0];
	
endmodule	   		