module mips(clk, rst) ;
    input clk ;   // clock
    input rst ;   // reset
    
    //各个模块的输出
    //controller
    wire [1:0]Regdst;//GPR写使能有效时，选目标寄存器00-rt，01-rd，10-31号
    wire ALUsrc;     //第二个操作数，0-GPR，1-Extender
    wire [1:0] MemtoReg; //写入寄存器的数据来源00-ALU，01-DM,10-NPC
    wire RegWrite;  //寄存器写使能
    wire MemWrite;  //dm写使能
    wire [1:0]ExtOp;//00-无符号，01-符号，10-高位置立即数
    wire [2:0]ALUOp; //000-比较，001-或，010-无溢出加法，011-有溢出加法，100-减法,101-来自扩展器原样输出，111-输出0
    wire flow_ctr;
    wire [2:0]NPCop;  //00-PC+4,01-beq&zero,10-j/jal,11-jr
    
    //im
    wire [31:0]dout_im;
    
    //GPR
    wire [31:0]outA;
    wire [31:0]outB;
    
    //dm
    wire [31:0]dout_dm;  // 32位输出
    
    //ALU
    wire [31:0]result;
    wire zero; //零标记
    wire overflow;  //溢出标记，写入30号寄存器0位置
    wire BLTZAL;
    
    //Extender
    wire [31:0] out_ex;
    
    //多路选择器
    wire [31:0] out2_1;
    wire [31:0] out3_1;
    wire [4:0] out_3;
    
    //NPC
    wire [31:0] NPC;  //下一条指令地址
    wire [31:0] jalAd;     //jal跳转前向31号寄存器存的地址
    
    //PC
    wire [31:0] PC;
    
    //Divider
    wire [5:0]op;
	  wire [4:0]rs;
	  wire [4:0]rt;
	  wire [4:0]rd;
	  wire [5:0]func;
	  wire [15:0]imm16;//beq跳转偏移
	  wire [25:0]imm26;//j、jal指令 26位跳转地址
    
    controller control(op,zero,func,overflow,BLTZAL,Regdst,ALUsrc,MemtoReg,RegWrite,MemWrite,ExtOp,ALUOp,flow_ctr,NPCop);
    
    im_1k i1(PC[9:0],dout_im);
    
    GPR gpr(clk,RegWrite,out_3,rs,rt,out3_1,flow_ctr,overflow,outA,outB);
  
    dm_1k dm(result[9:0],outB,MemWrite,clk,dout_dm );
    
    ALU alu(outA,out2_1,ALUOp,result,zero,overflow,BLTZAL);
    
    Extender extender(imm16,ExtOp,out_ex);
    
    multiplexer2_1 MUL2_1(ALUsrc,outB,out_ex,out2_1);   //ALU第二操作数
    
    multiplexer3_1 MUL3_1(MemtoReg,result,dout_dm,jalAd,out3_1); //存入GPR的数据来源
    
    multiplexer3_2 MUL3_2(Regdst,rt,rd,out_3);   //GPR写入位置
    
    NPC npc(PC,imm16,imm26,outA,NPCop,NPC,jalAd);
  
    PC pc(NPC,clk,rst,PC);
    
    Divider divider(dout_im,op,rs,rt,rd,func,imm16,imm26);
    
  endmodule