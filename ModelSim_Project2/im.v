module im_1k(
  input [9:0]addr,  //要输出的指令的地址
  output [31:0]dout
  );
     
  reg [7:0]im[1023:0];  //im读取到的指令
  assign dout={im[addr],im[addr+1],im[addr+2],im[addr+3]};//10位地址对应1024个字节空间
  
endmodule 
