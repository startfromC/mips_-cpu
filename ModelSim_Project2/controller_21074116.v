module controller(
  input clk,
  input [5:0] op,
  input zero,      //零标记
  input [5:0] func,
  input overflow,  //溢出标记
  //input BGEZAL_ctr,
  //input BLTZAL_ctr,
  
  output [1:0]Regdst,//GPR写使能有效时，选目标寄存器00-rt，01-rd，10-31号
  output ALUsrc,     //第二个操作数，0-GPR，1-Extender
  output [1:0] MemtoReg, //写入寄存器的数据来源00-ALU，01-DM,10-NPC
  output RegWrite,  //寄存器写使能
  output MemWrite,  //dm写使能
  output IRWrite,   //IR写使能
  output [1:0]ExtOp,//00-无符号，01-符号，10-高位置立即数
  output [2:0]ALUOp, //000-比较，001-或，010-无溢出加法，011-有溢出加法，100-减法
  output flow_ctr,  //指示GPR是否更新溢出标记
  output [2:0]NPCop,//000-PC+4,001-beq&zero,010-j/jal,011-jr,100-DIV
  output lb_ctr,
  output sb_ctr,
  output PC_ctr,
  output DIV_ctr
  );
  
  reg [3:0]fsm;
  wire S0,S1,S2,S3,S4,S5,S6,S7,S8,S9;
  assign S0=~fsm[3]&~fsm[2]&~fsm[1]&~fsm[0];  //0000
  assign S1=~fsm[3]&~fsm[2]&~fsm[1]&fsm[0];  //0001
  assign S2=~fsm[3]&~fsm[2]&fsm[1]&~fsm[0];  //0010
  assign S3=~fsm[3]&~fsm[2]&fsm[1]&fsm[0];  //0011
  assign S4=~fsm[3]&fsm[2]&~fsm[1]&~fsm[0];  //0100
  assign S5=~fsm[3]&fsm[2]&~fsm[1]&fsm[0];  //0101
  assign S6=~fsm[3]&fsm[2]&fsm[1]&~fsm[0];  //0110
  assign S7=~fsm[3]&fsm[2]&fsm[1]&fsm[0];  //0111
  assign S8=fsm[3]&~fsm[2]&~fsm[1]&~fsm[0];  //1000
  assign S9=fsm[3]&~fsm[2]&~fsm[1]&fsm[0];  //1001
  
  wire Rtype;
  wire addu,subu,ori,lw,sw,beq,j,lui,addi,addiu,slt,jal,jr,lb,sb/*,BGEZAL,BLTZAL*/;
  assign Rtype=~op[5]&~op[4]&~op[3]&~op[2]&~op[1]&~op[0];
  assign addu=Rtype&func[5]&~func[4]&~func[3]&~func[2]&~func[1]&func[0];
  assign subu=Rtype&func[5]&~func[4]&~func[3]&~func[2]&func[1]&func[0];
  assign ori=~op[5]&~op[4]&op[3]&op[2]&~op[1]&op[0];
  assign lw=op[5]&~op[4]&~op[3]&~op[2]&op[1]&op[0];
  assign sw=op[5]&~op[4]&op[3]&~op[2]&op[1]&op[0];
  assign beq=~op[5]&~op[4]&~op[3]&op[2]&~op[1]&~op[0];
  assign j=~op[5]&~op[4]&~op[3]&~op[2]&op[1]&~op[0];
  assign lui=~op[5]&~op[4]&op[3]&op[2]&op[1]&op[0];
  assign addi=~op[5]&~op[4]&op[3]&~op[2]&~op[1]&~op[0];
  assign addiu=~op[5]&~op[4]&op[3]&~op[2]&~op[1]&op[0];
  assign slt=Rtype&func[5]&~func[4]&func[3]&~func[2]&func[1]&~func[0];
  assign jal=~op[5]&~op[4]&~op[3]&~op[2]&op[1]&op[0];
  assign jr=Rtype&~func[5]&~func[4]&func[3]&~func[2]&~func[1]&~func[0];
  assign lb=op[5]&~op[4]&~op[3]&~op[2]&~op[1]&~op[0];
  assign sb=op[5]&~op[4]&op[3]&~op[2]&~op[1]&~op[0];
  assign DIV=Rtype&~func[5]&func[4]&func[3]&~func[2]&func[1]&~func[0];  //func:011010
  //assign BGEZAL=~op[5]&~op[4]&~op[3]&~op[2]&~op[1]&op[0];
  //assign BLTZAL=~op[5]&~op[4]&~op[3]&~op[2]&~op[1]&op[0];
  
  assign Regdst[1]=jal&S1|jal&S9|DIV&(S1|S6|S7);
  assign Regdst[0]=(addu|subu|slt)&S1|(addu|subu|slt)&S6|(addu|subu|slt)&S7|DIV&(S1|S6|S7);
  assign ALUsrc=(ori|lw|sw|lui|addi|addiu|lb|sb)&S1|(lw|sw|lb|sb)&S2|(lw|lb)&S3|(lw|lb)&S4|(sw|sb)&S5|(ori|lui|addi|addiu)&S6|(ori|lui|addi|addiu)&S7;
  assign MemtoReg[0]=(lw|lb)&S1|(lw|lb)&S2|(lw|lb)&S3|(lw|lb)&S4;
  assign MemtoReg[1]=jal&S1|jal&S9;
  assign RegWrite=(lw|lb)&S4|(addu|subu|ori|lui|addi&!overflow|addiu|slt)&S7|jal&S9|DIV&S7;
  assign MemWrite=(sw|sb)&S5;
  assign IRWrite=S0;//因为wire不能设定初值，所以启动时IR无法传送op  (addu|subu|ori|lw|sw|beq|j|lui|addi|addiu|slt|jal|jr|lb|sb)&S0
  assign ExtOp[0]=(lw|sw|addi|addiu|slt|lb|sb)&S1|(lw|sw|lb|sb)&S2|(lw|lb)&S3|(lw|lb)&S4|(sw|sb)&S5|(addi|addiu|slt)&S6|(addi|addiu|slt)&S7;
  assign ExtOp[1]=(lui)&S1|(lui)&S6|(lui)&S7;
  assign ALUOp[0]=(ori|addi)&S1|(ori|addi)&S6|(ori|addi)&S7|DIV&(S1|S6|S7);
  assign ALUOp[1]=(addu|lw|sw|lui|addi|addiu|lb|sb)&S1|(lw|sw|lb|sb)&S2|(lw|lb)&S3|(lw|lb)&S4|(sw|sb)&S5|(addu|lui|addi|addiu)&S6|(addu|lui|addi|addiu)&S7;
  assign ALUOp[2]=(subu|beq)&S1|(subu)&S6|(subu)&S7|(beq)&S8|DIV&(S1|S6|S7);
  assign flow_ctr=addi&(S1|S6|S7)&overflow;
  assign NPCop[0]=beq&(S1|S8)|jr&(S1|S9);
  assign NPCop[1]=(j|jal)&S1|(j|jal)&S9|jr&(S1|S9);
  assign NPCop[2]=1'b0;
  assign lb_ctr=lb&(S1|S2|S3|S4);
  assign sb_ctr=sb&(S1|S2|S5);
  assign PC_ctr=S0|beq&zero&S8|(j|jal|jr)&S9; //因为wire不能设定初值，所以启动时PC无法跳转至下一条 (addu|subu|ori|lw|sw|beq|j|lui|addi|addiu|slt|jal|jr|lb|sb)&S0
  assign DIV_ctr=DIV&(S1|S6|S7);
  
  always@(posedge clk)
  begin
    case(fsm)
      4'b0000:fsm<=4'b0001;
      4'b0001:
        begin
          if(lw||sw||lb||sb) fsm<=4'b0010;
          else if(Rtype&!jr||ori||lui||addi|addiu||DIV) fsm<=4'b0110;
          else if(beq) fsm<=4'b1000;
          else if(j||jal||jr) fsm<=4'b1001;
        end
      4'b0010:
        begin
          if(lb||lw) fsm<=4'b0011;
          else if(sb||sw) fsm<=4'b0101;
        end
      4'b0011:fsm<=4'b0100;
      4'b0100:fsm<=4'b0000;
      4'b0101:fsm<=4'b0000;
      4'b0110:fsm<=4'b0111;
      4'b0111:fsm<=4'b0000;
      4'b1000:fsm<=4'b0000;
      4'b1001:fsm<=4'b0000;
      default:fsm<=4'b0000;
    endcase
  end
endmodule    
  