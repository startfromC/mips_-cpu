module Register(  //寄存器组/ALU/DM之后的小寄存器
  input clk,
  input [31:0]data,
  output reg [31:0]out
  );
  
  always@(posedge clk)
    out=data;
endmodule
