module GPR(
  input clk,
  input RegWrite,//写使能
  
  input [4:0]rw,  //写入的寄存器编号
  input [4:0]rs,
  input [4:0]rt,
  input [31:0]din,   //要写入的数据
  input flow_ctr,    //是否更新溢出标记
  input overflow,    //溢出标记，写入30号寄存器第0位
  
  output [31:0]outA,
  output [31:0]outB
  );
  
  reg [31:0] RegFile[31:0];
  
  assign outA=RegFile[rs];
  assign outB=RegFile[rt];
  
  integer i;
  initial
    begin
      for(i=0;i<32;i=i+1)RegFile[i]<=0;
    end
    
  always@(posedge clk)
    begin
      if(RegWrite==1'b1&&rw!=5'b00000) RegFile[rw]<=din;
      if(flow_ctr==1'b1)  RegFile[30][0]<=overflow;  //溢出标记 
    end
endmodule
  