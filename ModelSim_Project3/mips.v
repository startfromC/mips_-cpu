module mips(clk, rst,PrDIn,HWInt,PrAddr,PrDOut,Wen) ;
    input clk ;   // clock
    input rst ;   // reset
    input [31:0] PrDIn;//从 Bridge 模块读入的数据
    input [7:2] HWInt;//6 个硬件中断请求
    
    output [31:0] PrAddr;
    output [31:0] PrDOut;	//输出至 Bridge 模块的数据
    output Wen;	//写允许信号（外设）

    
    //各个模块的输出
    //controller
    wire [1:0]Regdst;//GPR写使能有效时，选目标寄存器00-rt，01-rd，10-31号
    wire ALUsrc;     //第二个操作数，0-GPR，1-Extender
    wire [2:0] MemtoReg; //写入寄存器的数据来源00-ALU，01-DM,10-NPC
    wire RegWrite;  //寄存器写使能
    wire MemWrite;  //dm写使能
    wire [1:0]ExtOp;//00-无符号，01-符号，10-高位置立即数
    wire [2:0]ALUOp; //000-比较，001-或，010-无溢出加法，011-有溢出加法，100-减法,101-来自扩展器原样输出，111-输出0
    wire flow_ctr;
    wire [2:0]NPCop; //000-PC+4,001-beq,010-j/jal,011-jr,100-0x00004180,101-ERET
    wire lb_ctr;
    wire sb_ctr;
    wire PC_ctr;
    wire EXLSet;
    wire EXLClr;
    wire Wen_CP0;   //CP0写使能
    
    //im
    wire [31:0]dout_im;
    
    //GPR
    wire [31:0]outA;
    wire [31:0]outB;
    
    //register
    wire [31:0]A;
    wire [31:0]B;
    wire [31:0]ALU_out;
    wire [31:0]dm_out;
    
    //dm
    wire [31:0]dout_dm;  // 32位输出
    
    //dm_lb
    wire [31:0]out_dmlb;
    
    //ALU
    wire [31:0]result;
    wire zero; //零标记
    wire overflow;  //溢出标记，写入30号寄存器0位置
    
    //Extender
    wire [31:0] out_ex;
    
    //多路选择器
    wire [31:0] out2_1;
    wire [31:0] out5_1;
    wire [4:0] out_3;
    
    //NPC
    wire [31:0] NPC;  //下一条指令地址
    wire [31:0] jalAd;     //jal跳转前向31号寄存器存的地址
    
    //PC
    wire [31:0] PC;
    
    //IR
    wire [5:0]op;
	  wire [4:0]rs;
	  wire [4:0]rt;
	  wire [4:0]rd;
	  wire [5:0]func;
	  wire [15:0]imm16;//beq跳转偏移
	  wire [25:0]imm26;//j、jal指令 26位跳转地址
	  
	  //DM_ex
	  wire [31:0]dm_data;
	  
	  //CP0
	  wire [31:2]EPC;
    wire IntReq; //中断产生信号
    wire [31:0]dout_CP0;
    
    controller control(clk,op,rs,zero,func,overflow,IntReq,ALU_out,Regdst,ALUsrc,MemtoReg,RegWrite,MemWrite,IRWrite,ExtOp,ALUOp,flow_ctr,NPCop,lb_ctr,sb_ctr,PC_ctr,EXLSet,EXLClr,Wen_CP0,Wen);
    
    im_8k i1(PC[12:0],dout_im);
    
    GPR gpr(clk,RegWrite,out_3,rs,rt,out5_1,flow_ctr,overflow,outA,outB);
    
    Register register_A(clk,outA,A);
    
    Register register_B(clk,outB,B);
  
    dm_12k dm(ALU_out[13:0],dm_data,MemWrite,clk,dout_dm );
    
    DM_lb dm_lb(ALU_out[1:0],dout_dm,lb_ctr,out_dmlb);  //把处理lb的部分放在dm和数据寄存器之间
    
    Register dm_register(clk,out_dmlb,dm_out);
    
    ALU alu(A,out2_1,ALUOp,result,zero,overflow);
    
    Register ALUout(clk,result,ALU_out);
    
    Extender extender(imm16,ExtOp,out_ex);
    
    multiplexer2_1 MUL2_1(ALUsrc,B,out_ex,out2_1);   //ALU第二操作数
    
    multiplexer5_1 MUL5_1(MemtoReg,ALU_out,dm_out,jalAd,dout_CP0,PrDIn,out5_1); //存入GPR的数据来源
    
    multiplexer3_2 MUL3_2(Regdst,rt,rd,out_3);   //GPR写入位置
    
    NPC npc(PC,imm16,imm26,A,EPC,NPCop,NPC,jalAd);
  
    PC pc(clk,rst,PC_ctr,NPC,PC);
    
    IR ir(clk,dout_im,IRWrite,op,rs,rt,rd,func,imm16,imm26);
    
    DM_ex dm_ex(sb_ctr,dout_dm,B[7:0],ALU_out[1:0],B,dm_data);
    
    CP0 cp0(clk,rst,PC[31:2],B,HWInt,rd,Wen_CP0,EXLSet,EXLClr,EPC,IntReq,dout_CP0);
    
    assign PrAddr=ALU_out;
    assign PrDOut=B;
    
  endmodule