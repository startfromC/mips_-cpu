module NPC(
  input [31:0]PC,
  input [15:0]imm16,
  input [25:0]imm26,
  input [31:0]rs,  //存有jr指令要跳转地址的31号寄存器，jal跳转前向里面存入地址
  input [31:2]EPC,
  input [2:0]NPCop,//000-PC+4,001-beq,010-j/jal,011-jr,100-0x00004180,101-ERET
  
  output reg [31:0]NPC,
  output [31:0]jalAd  //jal要跳转的地址
  );
  
  reg [31:0]imm16_ex; //16位立即数符号扩展后补00
  assign jalAd=PC;
  
  always@(*)
    begin
      if(NPCop==3'b010) NPC<={PC[31:28],imm26,2'b00};
      else if(NPCop==3'b011) NPC<=rs;
      else if(NPCop==3'b001)
        begin
          if(imm16[15]==1'b0) imm16_ex={{14{1'b0}},imm16[15:0],2'b00};
          else if(imm16[15]==1'b1) imm16_ex={{14{1'b1}},imm16[15:0],2'b00};
          NPC<=PC+imm16_ex;
        end
      else if(NPCop==3'b000) NPC<=PC+4;
      else if(NPCop==3'b100) NPC<=32'h00004180;//中断处理程序地址
      else if(NPCop==3'b101) NPC<={EPC,2'b00};  //ERET          
    end
  endmodule
