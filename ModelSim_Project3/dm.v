module dm_12k( addr, din, we, clk, dout ) ;//dm只取一个字（末尾必定为00）忽略尾部第0和第1位
  input   [13:0]  addr ;  // 14位地址才能表示到12288字节空间
  input   [31:0]  din ;   // 32位输入
  input           we ;    // 写使能
  input           clk ;   // clock
  output  [31:0]  dout ;  // 32位输出
  reg     [7:0]  dm[12287:0] ;
  
  wire [13:0] address;
  
  assign address={addr[13:2],2'b00};
  assign dout={dm[address+3],dm[address+2],dm[address+1],dm[address]};
  
  integer i;
  initial
    begin
      for(i=0;i<12288;i=i+1)dm[i]<=0; //初始化数据存储器为0
    end
  
  always@(posedge clk)
    begin
      if(we) 
        begin
          dm[address]<=din[7:0];
          dm[address+1]<=din[15:8];
          dm[address+2]<=din[23:16];
          dm[address+3]<=din[31:24];
        end
    end
endmodule