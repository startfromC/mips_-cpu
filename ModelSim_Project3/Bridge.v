module Bridge(
  input [31:0]PrAddr,  //CPU提供的设备地址，来自ALUout
  input [31:0]PrWD,    //CPU写数据
  input WeCPU,     //外设写使能
  input TC_int,
  input in_int,
  input out_int,   //三个外设的中断请求
  input [31:0]TC_data,
  input [31:0]in_data,//外设向CPU传送的数据
  //input [31:0]out_data,
  
  output [2:0]addr,  //只设计了三个外设：定时器3个寄存器12字节96位；32位输入；32位输出；外设占地址空间共20字节,去除末尾00，最终只需要3位地址即可
  output [31:0]PrRD,  //CPU读数据
  output [31:0]Dev_data,  //CPU向外设输出数据
  output [5:0]HWInt,   //哪些外设申请中断 2-in,1-out,0-TC
  output WeTC,
  //output Wein,//输入设备，CPU只读
  output Weout       //外设写使能
  );
  
  wire TC,in,out;
  assign addr=PrAddr[4:2];  //每个字的起始地址末尾为00，addr表示最小精确到字的位置
  assign TC=(PrAddr[31:4]==28'h00007f0)&(PrAddr[3]==1'b0|PrAddr[3:2]==2'b10);  //TC的三个字空间地址最后4位为0000~0011,0100~0111,1000~1011
  assign in=(PrAddr[31:2]==30'b0000_0000_0000_0000_0111_1111_0000_11); //in的字空间地址最后2位为00~11
  assign out=(PrAddr[31:2]==30'b0000_0000_0000_0000_0111_1111_0001_00); //out的字空间地址最后2位为00~11
  assign HWInt={3'b0,in_int,out_int,TC_int};
  assign PrRD=(TC)?TC_data:(in)?in_data:32'h00000000;  //IN设备只读，OUT设备只写
  //assign PrRD=(TC)?TC_data:(in)?in_data:out_data;
  assign Dev_data=PrWD;
  assign WeTC=WeCPU&TC;
  //assign Wein=WeCPU&in;
  assign Weout=WeCPU&out;
  
endmodule