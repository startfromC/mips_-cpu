module Extender(
  input [15:0] in,
  input [1:0] ExtOp,
  output reg [31:0] out
  );
  always@(*)
    begin
      case(ExtOp)
        2'b00: //无符号扩展
          begin
            out={16'b0000_0000_0000_0000,in[15:0]};
          end
        2'b01://符号扩展
          begin
            if(in[15]==1'b1)
              out={16'b1111_1111_1111_1111,in[15:0]};
            else out={16'b0000_0000_0000_0000,in[15:0]};
          end
        2'b10://高位置立即数
          begin
            out={in[15:0],16'b0000_0000_0000_0000};
          end
      endcase
    end
endmodule