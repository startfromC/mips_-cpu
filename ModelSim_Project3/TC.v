module TC(
  input CLK_I, //时钟
  input RST_I,	//复位信号
  input [3:2]ADD_I, //地址输入 00-CTRL,01-PRESET,10_COUNT
  input WE_I, //写使能
  input [31:0]DAT_I,  //32 位数据输入
  
  output [31:0]DAT_O,    //32 位数据输出
  output IRQ//中断请求
);
  reg [31:0]CTRL;//控制寄存器	R/W	0
  //reserved	[31:4]
	//			im			[3]  //中断屏蔽0-禁止，1-允许
	//			mode		[2:1]	00:模式0	01:模式1
	//			enable		[0]		计数器使能0-停止，1-允许
  reg [31:0]PRESET;//初值寄存器	R/W	0
  reg [31:0]COUNT;//计数值寄存器	R	0
  
  assign IRQ=!((|COUNT)|CTRL[2]|CTRL[1])&CTRL[3];
  assign DAT_O=COUNT;
  
  always@(posedge CLK_I or posedge RST_I)
    begin
      if(RST_I)
        begin
          CTRL<={32{1'b0}};PRESET<={32{1'b0}};COUNT<={32{1'b0}};CTRL[3]<=1'b0;
        end
      if(WE_I&&ADD_I==2'b00)begin CTRL=DAT_I;end
      if(WE_I&&ADD_I==2'b01)begin PRESET=DAT_I;end
      if(CTRL[2:1]==2'b00)  //模式0
        begin
          if(COUNT=={32{1'b0}}) begin CTRL[0]=1'b0; end //倒数为0，停止计数
          if(CTRL[0])begin COUNT=COUNT-1'b1;end
          if(WE_I&&ADD_I==2'b01)begin COUNT=PRESET; CTRL[0]=1'b1;end  //加载初值，恢复计数
        end
      if(CTRL[2:1]==2'b01&&COUNT=={32{1'b0}})//模式1
        begin
          if(CTRL[0])begin COUNT=COUNT-1'b1;end  
          COUNT<=PRESET;CTRL[0]<=1'b1;
        end
    end
    
  endmodule 


