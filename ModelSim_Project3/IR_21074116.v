module IR(  //ָ��Ĵ���
  input clk,
  input [31:0] ins,
  input IRWrite,
  output reg[5:0]op,
	output reg[4:0]rs,
	output reg[4:0]rt,
	output reg[4:0]rd,
	output reg[5:0]func,
	output reg[15:0]imm16,//beq��תƫ��
	output reg[25:0]imm26//j��jalָ�� 26λ��ת��ַ
	);
	
	always@(posedge clk)
	 begin
	   if(IRWrite)
	     begin
	       op<=ins[31:26];
	       rs<=ins[25:21];
	       rt<=ins[20:16];
	       rd<=ins[15:11];
	       func<=ins[5:0];
	       imm16<=ins[15:0];
	       imm26<=ins[25:0];
	     end
	 end
endmodule	   		