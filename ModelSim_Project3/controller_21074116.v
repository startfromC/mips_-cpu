module controller(
  input clk,
  input [5:0] op,
  input [4:0] rs,  //MFC0为00000，MTC0为00100
  input zero,      //零标记
  input [5:0] func,
  input overflow,  //溢出标记
  input IntReq,  //中断响应信号
  input [31:0]address,  //ALU计算的地址，如果在00000000~00002FFF之间则为存储器地址，若大于00007F00则为外设地址
  
  output [1:0]Regdst,//GPR写使能有效时，选目标寄存器00-rt，01-rd，10-31号
  output ALUsrc,     //第二个操作数，0-GPR，1-Extender
  output [2:0] MemtoReg, //写入寄存器的数据来源000-ALU，001-DM,010-NPC,011-CP0,100-外设
  output RegWrite,  //寄存器写使能
  output MemWrite,  //dm写使能
  output IRWrite,   //IR写使能
  output [1:0]ExtOp,//00-无符号，01-符号，10-高位置立即数
  output [2:0]ALUOp, //000-比较，001-或，010-无溢出加法，011-有溢出加法，100-减法
  output flow_ctr,  //指示GPR是否更新溢出标记
  output [2:0]NPCop,//000-PC+4,001-beq,010-j/jal,011-jr,100-0x00004180,101-ERET
  output lb_ctr,
  output sb_ctr,
  output PC_ctr,
  output EXLSet,
  output EXLClr,
  output Wen_CP0,   //CP0写使能
  output We_Dev  //外设写使能
  );
  
  reg [3:0]fsm;
  wire S0,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10;
  assign S0=~fsm[3]&~fsm[2]&~fsm[1]&~fsm[0];  //0000
  assign S1=~fsm[3]&~fsm[2]&~fsm[1]&fsm[0];  //0001
  assign S2=~fsm[3]&~fsm[2]&fsm[1]&~fsm[0];  //0010
  assign S3=~fsm[3]&~fsm[2]&fsm[1]&fsm[0];  //0011
  assign S4=~fsm[3]&fsm[2]&~fsm[1]&~fsm[0];  //0100
  assign S5=~fsm[3]&fsm[2]&~fsm[1]&fsm[0];  //0101
  assign S6=~fsm[3]&fsm[2]&fsm[1]&~fsm[0];  //0110
  assign S7=~fsm[3]&fsm[2]&fsm[1]&fsm[0];  //0111
  assign S8=fsm[3]&~fsm[2]&~fsm[1]&~fsm[0];  //1000
  assign S9=fsm[3]&~fsm[2]&~fsm[1]&fsm[0];  //1001
  assign S10=fsm[3]&~fsm[2]&fsm[1]&~fsm[0]; //1010  中断处理(PC进入CP0，EXL置1）
  
  wire Rtype;
  wire addu,subu,ori,lw,sw,beq,j,lui,addi,addiu,slt,jal,jr,lb,sb,ERET,MFC0,MTC0;
  assign Rtype=~op[5]&~op[4]&~op[3]&~op[2]&~op[1]&~op[0];
  assign addu=Rtype&func[5]&~func[4]&~func[3]&~func[2]&~func[1]&func[0];
  assign subu=Rtype&func[5]&~func[4]&~func[3]&~func[2]&func[1]&func[0];
  assign ori=~op[5]&~op[4]&op[3]&op[2]&~op[1]&op[0];
  assign lw=op[5]&~op[4]&~op[3]&~op[2]&op[1]&op[0];
  assign sw=op[5]&~op[4]&op[3]&~op[2]&op[1]&op[0];
  assign beq=~op[5]&~op[4]&~op[3]&op[2]&~op[1]&~op[0];
  assign j=~op[5]&~op[4]&~op[3]&~op[2]&op[1]&~op[0];
  assign lui=~op[5]&~op[4]&op[3]&op[2]&op[1]&op[0];
  assign addi=~op[5]&~op[4]&op[3]&~op[2]&~op[1]&~op[0];
  assign addiu=~op[5]&~op[4]&op[3]&~op[2]&~op[1]&op[0];
  assign slt=Rtype&func[5]&~func[4]&func[3]&~func[2]&func[1]&~func[0];
  assign jal=~op[5]&~op[4]&~op[3]&~op[2]&op[1]&op[0];
  assign jr=Rtype&~func[5]&~func[4]&func[3]&~func[2]&~func[1]&~func[0];
  assign lb=op[5]&~op[4]&~op[3]&~op[2]&~op[1]&~op[0];
  assign sb=op[5]&~op[4]&op[3]&~op[2]&~op[1]&~op[0];
  assign ERET=~op[5]&op[4]&~op[3]&~op[2]&~op[1]&~op[0]&~func[5]&func[4]&func[3]&~func[2]&~func[1]&~func[0];//op:010000 func:011000//恢复PC和开中断
  assign MFC0=~op[5]&op[4]&~op[3]&~op[2]&~op[1]&~op[0]&~rs[4]&~rs[3]&~rs[2]&~rs[1]&~rs[0];//op:010000和ERET一样,rs:00000//读取CP0寄存器至通用寄存器
  assign MTC0=~op[5]&op[4]&~op[3]&~op[2]&~op[1]&~op[0]&~rs[4]&~rs[3]&rs[2]&~rs[1]&~rs[0];//op:010000,rs:00100//通用寄存器写入CP0
  
  wire dev;  //访问外设
  assign dev=(address>=32'h00007f00);
  
  assign Regdst[1]=jal&S1|jal&S9;
  assign Regdst[0]=(addu|subu|slt)&S1|(addu|subu|slt)&S6|(addu|subu|slt)&S7;
  assign ALUsrc=(ori|lw|sw|lui|addi|addiu|lb|sb)&S1|(lw|sw|lb|sb)&S2|(lw|lb)&S3|(lw|lb)&S4|(sw|sb)&S5|(ori|lui|addi|addiu)&S6|(ori|lui|addi|addiu)&S7;
  assign MemtoReg[0]=((lw|lb)&S1|(lw|lb)&S2|(lw|lb)&S3|(lw|lb)&S4|MFC0&(S1|S6))&!dev;
  assign MemtoReg[1]=jal&S1|jal&S9|MFC0&(S1|S6);
  assign MemtoReg[2]=dev&(lw&(S1|S2|S3|S4));//lw指令下如果地址在外设范围内，则取外设数据
  assign RegWrite=(lw|lb)&S4|(addu|subu|ori|lui|addi&!overflow|addiu|slt)&S7|jal&S9|MFC0&S6;
  assign MemWrite=(sw|sb)&S5;
  assign IRWrite=S0|S10;//因为wire不能设定初值，所以启动时IR无法传送op  (addu|subu|ori|lw|sw|beq|j|lui|addi|addiu|slt|jal|jr|lb|sb)&S0
  assign ExtOp[0]=(lw|sw|addi|addiu|slt|lb|sb)&S1|(lw|sw|lb|sb)&S2|(lw|lb)&S3|(lw|lb)&S4|(sw|sb)&S5|(addi|addiu|slt)&S6|(addi|addiu|slt)&S7;
  assign ExtOp[1]=(lui)&S1|(lui)&S6|(lui)&S7;
  assign ALUOp[0]=(ori|addi)&S1|(ori|addi)&S6|(ori|addi)&S7;
  assign ALUOp[1]=(addu|lw|sw|lui|addi|addiu|lb|sb)&S1|(lw|sw|lb|sb)&S2|(lw|lb)&S3|(lw|lb)&S4|(sw|sb)&S5|(addu|lui|addi|addiu)&S6|(addu|lui|addi|addiu)&S7;
  assign ALUOp[2]=(subu|beq)&S1|(subu)&S6|(subu)&S7|(beq)&S8;
  assign flow_ctr=addi&(S1|S6|S7)&overflow;
  assign NPCop[0]=beq&(S1|S8)|jr&(S1|S9)|ERET&(S1|S9);
  assign NPCop[1]=(j|jal)&S1|(j|jal)&S9|jr&(S1|S9);
  assign NPCop[2]=S10|ERET&(S1|S9);
  assign lb_ctr=lb&(S1|S2|S3|S4);
  assign sb_ctr=sb&(S1|S2|S5);
  assign PC_ctr=S0|beq&zero&S8|(j|jal|jr)&S9|S10|(ERET&S9); //因为wire不能设定初值，所以启动时PC无法跳转至下一条 (addu|subu|ori|lw|sw|beq|j|lui|addi|addiu|slt|jal|jr|lb|sb)&S0
  assign EXLSet=S10;
  assign EXLClr=ERET&(S1|S9);
  assign Wen_CP0=MTC0&S6;
  assign We_Dev=dev&(sw&S5);
  
  
  always@(posedge clk)
  begin
    case(fsm)
      4'b0000:fsm<=4'b0001;
      4'b0001:
        begin
          if(lw||sw||lb||sb) fsm<=4'b0010;
          else if(Rtype&!jr||ori||lui||addi||addiu||MFC0||MTC0) fsm<=4'b0110;
          else if(beq) fsm<=4'b1000;
          else if(j||jal||jr||ERET) fsm<=4'b1001;
        end
      4'b0010:
        begin
          if(lb||lw) fsm<=4'b0011;
          else if(sb||sw) fsm<=4'b0101;
        end
      4'b0011:fsm<=4'b0100;
      4'b0100:
        if(IntReq) fsm<=4'b1010;
        else fsm<=4'b0000;
      4'b0101:
        if(IntReq) fsm<=4'b1010;
        else fsm<=4'b0000;
      4'b0110:fsm<=4'b0111;
      4'b0111:
        if(IntReq) fsm<=4'b1010;
        else fsm<=4'b0000;
      4'b1000:
        if(IntReq) fsm<=4'b1010;
        else fsm<=4'b0000;
      4'b1001:
        if(IntReq) fsm<=4'b1010;
        else fsm<=4'b0000;
      4'b1010:
        fsm<=4'b0000;
      default:fsm<=4'b0000;
    endcase
  end
endmodule    
  