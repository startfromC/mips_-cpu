module ALU(
  input [31:0] A,
  input [31:0] B,
  input [2:0] ALUop,  //操作控制
  
  output [31:0]result,
  output zero, //零标记
  output overflow  //溢出标记，写入30号寄存器0位置
  );
  
  wire compare,ori,addiu,addi,subu;
  
  assign compare=~ALUop[2]&~ALUop[1]&~ALUop[0];  //000
  assign ori=~ALUop[2]&~ALUop[1]&ALUop[0];  //001
  assign addiu=~ALUop[2]&ALUop[1]&~ALUop[0];  //010
  assign addi=~ALUop[2]&ALUop[1]&ALUop[0]; //011
  assign subu=ALUop[2]&~ALUop[1]&~ALUop[0];  //100
  //assign BLTZAL=ALUop[2]&~ALUop[1]&ALUop[0]&(A[31]==1'b1); //101
  
  assign result=(compare)?(($signed(A)<$signed(B))?32'h0000_0001:32'h0000_0000):
                (ori)?A|B:
                (addiu)?A+B:
                (addi)?A+B:
                (subu)?A-B:32'h0000_0000;
  assign zero=(A==B)?1'b1:1'b0;
  assign overflow=addi&(A[31]==B[31]&&result[31]!=A[31]);
  
  /*initial overflow=1'b0;
  
  always@(*)
    begin
      case(ALUop)
        3'b000:   //比较，若A<B，输出1，A对应rs,B对应rt
          begin
          result<=($signed(A)<$signed(B))?32'h0000_0001:32'h0000_0000;
          end
        3'b001:   //或
          begin
          result<=A|B;
          end
        3'b010:   //addiu和addu,无溢出标记的加法
          begin
          result<=A+B;
          end
        3'b011:   //addi，有溢出标记的加法
          begin
          result=A+B;
          if(A[31]==B[31]&&result[31]!=A[31]) overflow=1'b1;  //溢出检测
          else overflow=1'b0;
          end
        3'b100:  //subu
          begin
          result<=A-B;
          end
        default:
          begin
          result<=32'h0000_0000;
          end
      endcase
       if(A==B) zero=1'b1;
       else zero=1'b0;
     end*/
endmodule
    
          
