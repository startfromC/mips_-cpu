module DM_ex(  //处理sb指令下dm的操作和其他情况下的存入
  input sb_ctr,
  input [31:0]origin_data,
  input [7:0]plus,
  input [1:0]addr, //地址
  input [31:0]outB,  //其他情况下的写入数据
  
  output [31:0]data
  );
  
  reg [31:0]temp;
  
  assign data=temp;
  
  always@(*)
  begin
    if(sb_ctr==1'b1)
      begin
        temp=origin_data;
        temp[addr*8]<=plus[0];
        temp[addr*8+1]<=plus[1];
        temp[addr*8+2]<=plus[2];
        temp[addr*8+3]<=plus[3];
        temp[addr*8+4]<=plus[4];
        temp[addr*8+5]<=plus[5];
        temp[addr*8+6]<=plus[6];
        temp[addr*8+7]<=plus[7];
      end
    else
      temp=outB;
  end
endmodule
