module DM_lb(  //进行lb指令和其他情况下的内存读出
  input [1:0]address,
  input [31:0]data,
  input lb_ctr,
  
  output [31:0]out
);

  reg [7:0]temp;  
  
  assign out=(!lb_ctr)?data:(data[address*8+7])?{{24{1'b1}},temp}:{{24{1'b0}},temp};
  
  always@(*)
  temp={data[address*8+7],data[address*8+6],data[address*8+5],data[address*8+4],
  data[address*8+3],data[address*8+2],data[address*8+1],data[address*8]};
  
endmodule