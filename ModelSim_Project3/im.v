module im_8k(
  input [12:0]addr,  //要输出的指令的地址
  output [31:0]dout
  );
     
  reg [7:0]im[8191:0];  //im读取到的指令
  wire [12:0]address;
  assign address=addr-13'b1_0000_0000_0000;
  assign dout={im[address],im[address+1],im[address+2],im[address+3]};//13位地址对应8192个字节空间
  
endmodule 
