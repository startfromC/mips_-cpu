module CP0(
  input clk,
  input rst,
  input [31:2]PC,
  input [31:0]din,
  input [5:0]HWInt, //6个设备中断
  input [4:0]sel, //SR-12,CAUSE-13,EPC-14,PrID-15
  input Wen,  //写使能
  input EXLSet,  //EXL置1
  input EXLClr,  //EXL置0
  
  output [31:2]EPC,
  output IntReq,  //中断产生信号
  output [31:0]dout
  );
  
  reg [31:2]epc;  //保存中断时的PC
  reg [15:10] im ;//SR控制哪些外设允许中断
  reg exl,ie ;     //SR整体表示为：{16’b0, im, 8’b0, exl, ie}ie(全局中断允许),exl(已进入中断)
  reg [15:10]hwint_pend;  //Cause记录产生中断请求的外设//Cause整体表示为：{16’b0, hwint_pend, 10’b0}
  reg [31:0]PrID;
  
  initial 
    begin 
      PrID=32'h21074116;
      im=6'b000000;
      exl=1'b0;
      ie=1'b0;
    end
  
  assign IntReq=(|(HWInt&im))&ie&!exl;
  assign dout=(sel==12)?{{16{1'b0}},im,{8{1'b0}},exl,ie}:(sel==13)?{{16{1'b0}},hwint_pend,{10{1'b0}}}:(sel==14)?{epc,2'b00}:(sel==15)?PrID:{32{1'b0}};
  assign EPC=epc;
  
  always@(posedge clk)
  begin
    hwint_pend<=HWInt;
    if(Wen&&sel==4'b1100) {im,exl,ie}<={din[15:10],din[1],din[0]};
    //一般情况下不通过指令对Cause和EPC寄存器进行写入，而是硬件自动写入
    if(Wen&&sel==4'b1111) PrID<=din;
    if(EXLSet)begin exl<=1'b1; epc<=PC; end  //当产生有效中断且控制器进入中断处理状态，设置EXL=1，保存PC值
    if(EXLClr)exl<=1'b0;
  end
endmodule
  
  