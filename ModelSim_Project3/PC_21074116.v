module PC(
  input clk,
  input reset,
  input PC_ctr, //PC写使能，也是NPC的写使能
  input [31:0] NPC,
  output reg [31:0] PC
  );
  
  initial
    begin
      PC<=32'h0000_3000;
    end
    
  always@(posedge clk,posedge reset)
    begin
      if(PC_ctr)
        begin
          if(reset) PC<=32'h0000_3000;
          else PC<=NPC;
        end
    end
endmodule
            