module IN(
  input [31:0]din,
  input we,
  output in_int,
  output [31:0]dout
  );
  reg [31:0]in;
  
  initial in=32'h0000_0032;  //�Ĵ�����ֵΪ50
  
  assign dout=in;
  assign in_int=1'b0;
  
  always@(*)
    if(we) in=din;
  
endmodule