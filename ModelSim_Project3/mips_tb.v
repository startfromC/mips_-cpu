module mips_tb;
	reg clk;
	reg reset;
	reg [31:0]din_in=32'h000000032;  //输入信号，用来修改输入设备的值
	
	wire [31:0]PrDIn;
	wire [7:2]HWInt;
  wire [31:0] PrAddr;
  wire [31:0] PrDOut;	//输出至 Bridge 模块的数据
  wire Wen;	//写允许信号（外设）
  wire TC_int;
  wire in_int;
  wire out_int;
  wire[31:0]dout_TC;
  wire [31:0]dout_in;
  wire [31:0]dout_out;
  wire [2:0]addr;
  wire[31:0]Dev_data;
  wire WeTC;
  /*wire*/ reg Wein=1'b0;
  wire Weout;
	
	initial 
	 begin
	   clk = 1;
	   reset=0;
	   #5 reset=1;
	   #5 reset=0;
     $readmemh("main.txt",mymips.i1.im); //文件读取指令
     $readmemh("main2.txt",mymips.i1.im,32'h00001180);
	 end
	   
	always #20 clk = ~clk;
	always #10000 din_in=din_in+2;
	always #10001 Wein=1;
	always #10002 Wein=0;
	
		mips mymips(clk,reset,PrDIn,HWInt,PrAddr,PrDOut,Wen);
		Bridge bridge(PrAddr,PrDOut,Wen,TC_int,in_int,out_int,dout_TC,dout_in/*,dout_out*/,addr,PrDIn,Dev_data,HWInt,WeTC,/*Wein,*/Weout);
		IN in(/*Dev_data*/din_in,Wein,in_int,dout_in);
		OUT out(Dev_data,Weout,out_int,dout_out);
		TC tc(clk,reset,addr[1:0],WeTC,Dev_data,dout_TC,TC_int);
		
endmodule
