module OUT(
  input [31:0]din,
  input We,
  output out_int,
  output [31:0]dout
  );
  reg [31:0]out;
  
  initial out=32'h00000000;
  
  assign out_int=1'b0;
  assign dout=out;
  
  always@(*)
    if(We) out=din;
  
endmodule
